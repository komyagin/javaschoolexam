package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.Objects;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        } else if (x.size() > y.size() || !x.isEmpty() && y.isEmpty()) {
            return false;
        } else if ((x.isEmpty() && !y.isEmpty()) || (x.isEmpty() && y.isEmpty())) {
            return true;
        } else {
            int remainder = x.size();
            int iterator = 0;
            for (Object object : x) {
                for (int i = iterator; i < y.size(); i++) {
                    if (object.equals(y.get(i))) {
                        remainder--;
                        iterator = i;
                        break;
                    }
                }
            }
            if (remainder == 0) {
                return true;
            }
            return false;
        }
    }
}
