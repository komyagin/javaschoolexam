package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    public int dimensionOfArray(List<Integer> list) {
        int size = list.size();
        int dimension = 0;
        for (int i = 1; i < list.size(); i++) {
            size -= i;
            dimension++;
            if (size == 0) {
                break;
            } else if (size < 0) {
                throw new CannotBuildPyramidException();
            }
        }
        return dimension;
    }

    public int[][] newMatrix(int dimension) {
        int m = dimension + (dimension - 1);
        int[][] matrix = new int[dimension][m];
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < m; j++) {
                matrix[i][j] = 0;
            }
        }
        return matrix;
    }

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }
        int dimension = dimensionOfArray(inputNumbers);
        int[][] matrix = newMatrix(dimension);
        Collections.sort(inputNumbers);
        int iterator = inputNumbers.size() - 1;
        int index = 0;
        int rows = dimension - 1;
        int columns = dimension + (dimension - 1) - 1;
        for (int i = rows; i >= 0; i--) {
            for (int j = columns--; j >= index; j -= 2) {
                matrix[i][j] = inputNumbers.get(iterator);
                iterator--;
            }
            index++;
        }
        return matrix;
    }
}
