package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Stack;

public class Calculator {

    private static class Value {
        private StringBuilder value = new StringBuilder();
        private boolean isFinished = false;

        public Value() {
        }

        public Value(String v) {
            value.append(v);
            isFinished = true;
        }

        public void append(char c) {
            if (isFinished)
                throw new IllegalArgumentException("Append data to finished value");
            else
                value.append(c);
        }

        @Override
        public String toString() {
            return "Value{" +
                    "value=" + value +
                    '}';
        }

        public float toFloat() {
            return Float.parseFloat(value.toString());
        }
    }

    private static class Operator {
        private char op;
        private int priority;

        public Operator(char op, int priority) {
            this.op = op;
            this.priority = priority;
        }

        @Override
        public String toString() {
            return "Operator{" +
                    "op=" + op +
                    ", priority=" + priority +
                    '}';
        }

        public float apply(float b, float a) {
            //System.out.println("Apply " + a + " " + op + " " + b);
            switch (op) {
                case '+':
                    return a + b;
                case '-':
                    return a - b;
                case '*':
                    return a * b;
                case '/':
                    return b == 0 ? null : a / b;
                default:
                    throw new IllegalArgumentException("No apply for " + op);
            }
        }

    }

    private static void evaluate(Stack<Value> values, Stack<Operator> operators, int priority) {
        while (!operators.isEmpty() && operators.peek().priority >= priority) {
            values.push(new Value(
                    String.valueOf(operators.pop().apply(
                            values.pop().toFloat(),
                            values.pop().toFloat()
                    ))
            ));
        }
        // remove open parenthesis if evaluate after close parenthesis (use specific priority)
        if (priority == -1 && !operators.isEmpty() && operators.peek().priority < priority)
            operators.pop();
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {
        try {
            Stack<Value> values = new Stack<>();
            values.push(new Value()); // starts with first value
            Stack<Operator> operators = new Stack<>();

            for (char c : statement.toCharArray()) {
                switch (c) {
                    case '-':
                    case '+':
                    case '*':
                    case '/':
                        int priority = (c == '*' || c == '/') ? 1 : 0;
                        evaluate(values, operators, priority);
                        values.push(new Value());
                        operators.push(new Operator(c, priority));
                        break;
                    case '(':
                        operators.push(new Operator(c, -2));
                        break;
                    case ')':
                        evaluate(values, operators, -1);
                        break;
                    case ',':
                        return null;
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                    case '.':
                        values.peek().append(c);
                }
            }
            evaluate(values, operators, 0);
            if (!operators.isEmpty() || values.size() != 1) {
                return null;
            }
            float result = values.pop().toFloat();
            DecimalFormat format = new DecimalFormat("0.####");
            return format.format(result);
        } catch (Exception ignore) {
            return null;
        }
    }
}
